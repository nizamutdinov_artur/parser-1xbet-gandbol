﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace gandbolparser_sharp
{

    public enum InfoType
    {
        
    }

    public class InfoDispencer
    {
        public GameModel.Root gameState;

        public InfoDispencer(GameModel.Root gameState)
        {
            this.gameState = gameState;
        }

        public InfoDispencer()
        {
        }

        public string Get()
        {
            return "";
        }


        public string Date(Game game)
        {
            return game.GameDate + ";";
        }

        public string Time(Game game)
        {
            return game.GameDateTime + ";";
        }

        public string Id(Game game)
        {
            return game.Id + ";";
        }

        public string Name(Game game)
        {
            return game.Name + ";";
        }

        public string GameMinute()
        {
            return gameState.Value.SC.TS / 60+ ";";
        }

        public string Score()
        {
            return gameState.Value.SC.FS.S1 + ":" + gameState.Value.SC.FS.S2 + ";";
        }

        public string C1x2()
        {
            string result = string.Empty;

            result += GetCoefficient(gameState.Value.E, 1); // "1x2слева;" 
            result += GetCoefficient(gameState.Value.E, 2); // "X;" 
            result += GetCoefficient(gameState.Value.E, 3); // "1x2 справа;" 

            return result;
        }

        public string Total()
        {
            string result = string.Empty;

            result += GetInfo(gameState.Value.E, 9, 3); // Т м; Т ср; Т б;

            result += GetCoeficients(gameState.Value.E, 9, ";", 3); // ТБ м; ТБ ср; ТБ б;
            result += GetCoeficients(gameState.Value.E, 10, ";", 3); // "ТМ м; ТМ ср; ТМ б" 

            return result;
        }

        public string Fora1()
        {
            string result = string.Empty;

            result += GetInfo(gameState.Value.E, 7, 2); // Фора 1 м; Фора 1 б

            result += GetCoeficients(gameState.Value.E, 7, ";", 2); // "ФОРА1;" 

            return result;
        }

        public string Fora2()
        {
            string result = string.Empty;

            result += GetInfo(gameState.Value.E, 8, 2); // Фора 2 м; Фора 2 б

            result += GetCoeficients(gameState.Value.E, 8, ";", 2); // "ФОРА2;" 

            return result;
        }

        public string IndividualTotal1()
        {
            string result = string.Empty;

            // "ИндТотал1 М;" +
            // "ИндТотал1 Б;" +
            // "ИндТотал1 М кф;" +
            // "ИндТотал1 Б кф;" + 
            result += GetInfo(gameState.Value.E, 11, 1);
            result += GetCoefficient(gameState.Value.E, 11);

            result += GetInfo(gameState.Value.E, 12, 1);
            result += GetCoefficient(gameState.Value.E, 12);

            return result;
        }

        public string IndividualTotal2()
        {
            string result = string.Empty;

            // "ИндТотал2 М;" +
            // "ИндТотал2 Б;" +
            // "ИндТотал2 М кф;" +
            // "ИндТотал2 Б кф;" +
            result += GetInfo(gameState.Value.E, 13, 1);
            result += GetCoefficient(gameState.Value.E, 13);

            result += GetInfo(gameState.Value.E, 14, 1);
            result += GetCoefficient(gameState.Value.E, 14);

            return result;
        }

        public string TotalChet()
        {
            string result = string.Empty;

            result += GetCoefficient(gameState.Value.E, 182);
            result += GetCoefficient(gameState.Value.E, 183);

            return result;
        }


        public string DoubleChance()
        {
            string result = string.Empty;

            result += GetCoefficient(gameState.Value.E, 4); // "Двойнойшанс1x;"
            result += GetCoefficient(gameState.Value.E, 5); // "Двойнойшанс12;"
            result += GetCoefficient(gameState.Value.E, 6); // "Двойнойшанс2x"

            return result;
        }


        private string GetCoefficient(List<GameModel.E> e, int t)
        {
            var outString = string.Empty;

            foreach (var element in e)
            {
                if (element.T == t)
                {
                    outString += element.C + ";";
                }

            }
            return outString;
        }

        private string GetCoeficients(dynamic e, int t, string delimiter, int minCount)
        {
            var values = new List<string>();
            var result = string.Empty;


            foreach (var element in e)
            {
                if (element.T == t)
                    values.Add(element.C + delimiter);
            }

            for (int i = 0; i < minCount - values.Count; i++)
                result += ";";


            return result;
        }


        private string GetInfo(List<GameModel.E> e, int t, int minCount)
        {
            var values = (from element in e where element.T == t where element.P != null select (double)element.P).ToList();

            string result = string.Empty;
            foreach (var value in values)
                result = result + (value + ";");

            if (values.Count >= minCount)
                return result;


            for (int i = 0; i < minCount - values.Count; i++)
                result += ";";
            return result;
        }


    }
}
