﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gandbolparser_sharp
{
    public partial class Form1 : Form
    {
        private Parser parser;
        private Export export;


        public Form1()
        {
            InitializeComponent();
            export = new Export();

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            parser = new Parser("https://1xstavka.ru");

            parser.TextUpdated += TextUpdated;
            parser.UpdateStat += EditControlText;
        }

        private void TextUpdated(string str)
        {
            richTextBox1.AppendText(str + "\n");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            parser.Update();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            export.PerformCSV(parser.GameController.Games);
        }



        public void EditControlText(string name, string text)
        {
           Control[] controls = Controls.Find(name, true) ;

            if (controls.Length >0)
                controls[0].Text = text;
        }
    }
}
