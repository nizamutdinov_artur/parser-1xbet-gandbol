﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gandbolparser_sharp
{
    public class Export
    {

        public Export()
        {
            if (Directory.GetCurrentDirectory() + "\\Output\\" != null)
                Directory.CreateDirectory(Directory.GetCurrentDirectory() + "\\Output\\");
        }

        public void PerformCSV(List<Game> games)
        {
                     
            Random random = new Random();
            InfoDispencer infoDispencer = new InfoDispencer();

            var path = (Directory.GetCurrentDirectory() + 
                "\\Output\\" +
                + random.Next(1, 1000)
                + ".csv");
            

            File.WriteAllText(path, "");


            List<string> outString = new List<string>();

            // Init
            if(File.ReadAllText(path).Length == 0)
            outString.Add("Дата;" +
                          "Время;" +
                          "Номер;" +
                          "Имя;" +
                          "Минута;" +
                          "Счет;" +
                          "1x2слева;" +
                          "X;" +
                          "1x2 справа;" +

                          "Т м;" +
                          "Т ср;" +
                          "Т б;" +

                          "ТБ м;" +
                          "ТБ ср;" +
                          "ТБ б;" +

                          "ТМ м;" +
                          "ТМ ср;" +
                          "ТМ б;" +

                          "Фора1 1;"+
                          "Фора1 2;"+
                          "Фора1 кф1;" +
                          "Фора1 кф2;" +

                          "Фора2 1;" +
                          "Фора2 2;" +
                          "Фора2 кф1;" +
                          "Фора2 кф2;" +

                          "ИндТотал1 М;" +
                          "ИндТотал1 Б;" +
                          "ИндТотал1 М кф;" +
                          "ИндТотал1 Б кф;" +

                          "ИндТотал2 М;" +
                          "ИндТотал2 Б;" +
                          "ИндТотал2 М кф;" +
                          "ИндТотал2 Б кф;"   +

                          "ТоталЧет да;" +
                          "ТоталЧет нет;" +

                          "Двойнойшанс1x;" +
                          "Двойнойшанс12;" +
                          "Двойнойшанс2x;");

            foreach (var game in games)
            {
                foreach (var gameState in game.GameStates)
                {
                    if (gameState == null)
                        continue;

                    infoDispencer.gameState = gameState;

                    string currentStateString =  string.Empty;
                                                            
                    currentStateString += infoDispencer.Date(game);
                    currentStateString += infoDispencer.Time(game);
                    currentStateString += infoDispencer.Id(game);
                    currentStateString += infoDispencer.Name(game);
                    currentStateString += infoDispencer.GameMinute();
                    currentStateString += infoDispencer.Score();

                    currentStateString += infoDispencer.C1x2();

                    currentStateString += infoDispencer.Total();

                    currentStateString += infoDispencer.Fora1();
                    currentStateString += infoDispencer.Fora2();

                    currentStateString += infoDispencer.IndividualTotal1();
                    currentStateString += infoDispencer.IndividualTotal2();

                    currentStateString += infoDispencer.TotalChet();

                    currentStateString += infoDispencer.DoubleChance();

                    #region Old

                    //game.GameDate + ";" +
                    //game.GameDateTime + ";" +
                    //game.Id + ";" +
                    //game.Name + ";";
                    //    GetCFValue(gameState.Value.E, 1); // "1x2слева;" 
                    //    GetCFValue(gameState.Value.E, 2); // "X;" 
                    //GetCFValue(gameState.Value.E, 3); // "1x2 справа;" 


                    //    GetPValues(gameState.Value.E, 9, 3) ; // Т м; Т ср; Т б;

                    //    GetCFValue(gameState.Value.E, 9, ";", 3) + // ТБ м; ТБ ср; ТБ б;
                    //    GetCFValue(gameState.Value.E, 10, ";", 3) + // "ТМ м; ср; б" 


                    //    GetPValues(gameState.Value.E, 7, 2) + // Фора 1 зн {2}

                    //    GetCFValue(gameState.Value.E, 7, ";", 2) + // "ФОРА1;" 


                    //    GetPValues(gameState.Value.E, 8, 2) + // Фора 2 зн {2}

                    //    GetCFValue(gameState.Value.E, 8, ";", 2) + // "ФОРА2;" 


                    //    // "ИндТотал1;" 
                    //    GetPValues(gameState.Value.E, 11, 1) +
                    //    GetCFValue(gameState.Value.E, 11) +

                    //    GetPValues(gameState.Value.E, 12, 1) +
                    //    GetCFValue(gameState.Value.E, 12) +


                    //    // "ИндТотал2;" 
                    //    GetPValues(gameState.Value.E, 13, 1) +
                    //    GetCFValue(gameState.Value.E, 13) +

                    //    GetPValues(gameState.Value.E, 14, 1) +
                    //    GetCFValue(gameState.Value.E, 14) +


                    //    // "ТоталЧет;"
                    //    GetCFValue(gameState.Value.E, 182) +
                    //    GetCFValue(gameState.Value.E, 183) +

                    //    GetCFValue(gameState.Value.E, 4) + // "Двойнойшанс1x;"
                    //    GetCFValue(gameState.Value.E, 5) + // "Двойнойшанс12;"
                    //    GetCFValue(gameState.Value.E, 6);  // "Двойнойшанс2x"     
                    #endregion

                    outString.Add(currentStateString);
                }

            }
            try
            {
                File.WriteAllLines(path, outString, Encoding.UTF8);
            }
            catch (Exception e)
            {
                MessageBox.Show("Произошла ошибка!\n" + e, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private string GetPValues(List<GameModel.E> e, int t, int minCount)
        {            
            var values = (from element in e where element.T == t where element.P != null select (double) element.P).ToList();

            string result = string.Empty;
            foreach (var value in values)
                result = result + (value + ";");

            if (values.Count >= minCount)
                return result;


            for (int i = 0; i < minCount - values.Count; i++)
                result += ";";
            return result;
        }

        private string GetCFValue(List<GameModel.E> e, int t)
        {
            var outString = string.Empty;

            foreach (var element in e)
            {
                if (element.T == t)
                {
                    outString += element.C + ";";
                }

            }
            return outString;
        }

        private string GetCFValues(dynamic e, int t, string delimiter)
        {
            var outString = string.Empty;
                 

            foreach (var element in e)
            {
                if (element.T == t)
                    outString = outString + element.C + delimiter;
            }

            if (outString.Length <= delimiter.Length || outString == String.Empty)
                return String.Empty;

            return outString;
        }


        private string GetCFValue(dynamic e, int t, string delimiter, int minCount)
        {
            var values = new List<string>();
            var result = string.Empty;


            foreach (var element in e)
            {
                if (element.T == t)
                    values.Add(element.C + delimiter);
            }

            for (int i = 0; i < minCount - values.Count; i++)
                result += ";";


            return result;
        }


    }
}
