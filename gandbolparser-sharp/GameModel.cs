﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace gandbolparser_sharp
{

    public class GameModel
    {                  
        public partial class Root
        {
            [JsonProperty("ErrorCode")]
            public long ErrorCode { get; set; }

            [JsonProperty("Id")]
            public long Id { get; set; }

            [JsonProperty("Error")]
            public string Error { get; set; }

            [JsonProperty("Guid")]
            public string Guid { get; set; }

            [JsonProperty("Success")]
            public bool Success { get; set; }

            [JsonProperty("Value")]
            public Value Value { get; set; }
        }

        public partial class Value
        {
            [JsonProperty("F")]
            public bool F { get; set; }

            [JsonProperty("L")]
            public string L { get; set; }

            [JsonProperty("O1E")]
            public string O1E { get; set; }

            [JsonProperty("EC")]
            public long EC { get; set; }

            [JsonProperty("COI")]
            public long COI { get; set; }

            [JsonProperty("CO")]
            public long CO { get; set; }

            [JsonProperty("E")]
            public List<E> E { get; set; }

            [JsonProperty("GE")]
            public List<GE> GE { get; set; }

            [JsonProperty("EGC")]
            public long EGC { get; set; }

            [JsonProperty("I")]
            public long I { get; set; }

            [JsonProperty("MIS")]
            public List<object> MIS { get; set; }

            [JsonProperty("LI")]
            public long LI { get; set; }

            [JsonProperty("LE")]
            public string LE { get; set; }

            [JsonProperty("MIO")]
            public MIO MIO { get; set; }

            [JsonProperty("O1")]
            public string O1 { get; set; }

            [JsonProperty("N")]
            public long N { get; set; }

            [JsonProperty("O1C")]
            public long O1C { get; set; }

            [JsonProperty("O2E")]
            public string O2E { get; set; }

            [JsonProperty("SC")]
            public SC SC { get; set; }

            [JsonProperty("O2")]
            public string O2 { get; set; }

            [JsonProperty("O1I")]
            public long O1I { get; set; }

            [JsonProperty("O2C")]
            public long O2C { get; set; }

            [JsonProperty("R")]
            public long R { get; set; }

            [JsonProperty("O2I")]
            public long O2I { get; set; }

            [JsonProperty("S")]
            public long S { get; set; }

            [JsonProperty("SI")]
            public long SI { get; set; }
            [JsonProperty("VA")]
            public long VA { get; set; }
            [JsonProperty("SE")]
            public string SE { get; set; }
            [JsonProperty("SN")]
            public string SN { get; set; }
            [JsonProperty("VI")]
            public string VI { get; set; }
        }

        public partial class E
        {
            [JsonProperty("C")]
            public double C { get; set; }

            [JsonProperty("G")]
            public long G { get; set; }

            [JsonProperty("B")]
            public bool B { get; set; }

            [JsonProperty("CE")]
            public long? CE { get; set; }

            [JsonProperty("P")]
            public double? P { get; set; }

            [JsonProperty("T")]
            public long T { get; set; }
        }

        public partial class GE
        {
            [JsonProperty("E")]
            public List<List<E>> E { get; set; }

            [JsonProperty("G")]
            public long G { get; set; }
        }

        public partial class MIO { }

        public partial class SC
        {
            [JsonProperty("PS")]
            public List<object> PS { get; set; }

            [JsonProperty("FS")]
            public FS FS { get; set; }

            [JsonProperty("S")]
            public List<Empty> S { get; set; }

            [JsonProperty("TS")]
            public long TS { get; set; }
        }

        public partial class FS
        {
            [JsonProperty("S1")]
            public long S1 { get; set; }

            [JsonProperty("S2")]
            public long S2 { get; set; }
        }

        public partial class Empty
        {
            [JsonProperty("Key")]
            public string Key { get; set; }

            [JsonProperty("Value")]
            public string Value { get; set; }
        }

        public partial class Root
        {
            public static Root FromJson(string json)
            {
                return JsonConvert.DeserializeObject<Root>(json, Converter.Settings);
            }
        }

        public static class Serialize
        {
            public static string ToJson(Root self)
            {
                return JsonConvert.SerializeObject(self, Converter.Settings);
            }
        }

        public class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
            };
        }
      
    }


}