﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gandbolparser_sharp
{           
    public class Game
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string GameDate { get; set; }
        public string GameDateTime { get; set; }

        public GameModel.Root[] GameStates = new GameModel.Root[3];

        public Game(GameModel.Root gameState)
        {
            Id = (int)gameState.Value.I;
            Name = GetGameName(gameState) ;

            GameDate = DateTime.Now.ToShortDateString();
            GameDateTime = DateTime.Now.ToShortTimeString();
        }

        public void AddGameState(GameModel.Root gameState, int gameStateIndex)
        {
            GameStates[gameStateIndex] = gameState;
        }

           
        public int GetLastGameIndex()
        {
            for (int i = GameStates.Length - 1; i >= 0; i--)
                if (GameStates[i] != null)
                    return i;
                   
            for (var i = 0; i < GameStates.Length; i++)
                if (GameStates[i] == null) { return i - 1; }

            return -1;
        }

        public string GetScore()
        {
            if (GetLastGameIndex() == -1)
                return string.Empty;
            
            return GameStates[GetLastGameIndex()].Value.SC.FS.S1 + ":" + GameStates[GetLastGameIndex()].Value.SC.FS.S2;
        }
      

        #region  НеЛазь

        private string GetGameName(GameModel.Root gameState)
        {
            return gameState.Value.O1E + "-" + gameState.Value.O2E;
        }

        #endregion

    }
          
}
