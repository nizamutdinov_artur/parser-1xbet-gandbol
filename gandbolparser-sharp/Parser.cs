﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using RestSharp.Extensions;

namespace gandbolparser_sharp
{
   public class Parser
    {
        public delegate void parserDelegate(string str);
        public delegate void parserStatisticDelegate(string name, string text);



        public GameController GameController;

        public TelegramBot TelegramBot;

        private RestClient restClient;
        private string baseUri;

        public parserDelegate TextUpdated;
        public parserStatisticDelegate UpdateStat;

        public string BaseURI
        {
            get { return baseUri; }
            set
            {
                baseUri = value;
                restClient.BaseUrl = new Uri(value);
            }
        }

        public Parser(string baseURI)
        {
            baseUri = baseURI;
            restClient = new RestClient(baseURI);
            GameController = new GameController();

            //TelegramBot = new TelegramBot();

            //TelegramBot.SendMessage("zaebis krasava super love you", 219370133);


            //TextUpdated += str =>
            //{
            //    TelegramBot.SendMessage(str, 219370133);
            //};
        }

        public dynamic GetJsonFromLink (string link)
        {
            RestRequest request = new RestRequest(link, Method.GET);
            IRestResponse response = restClient.Execute(request);
            return Newtonsoft.Json.JsonConvert.DeserializeObject(response.Content);
        }

        public string GetLigaId()
        {
            try {
                foreach (var value in GetJsonFromLink("LiveFeed/GetSportsShortZip?sports=100&cyberFlag=1&country=1").Value)
                {
                    // Liga Id
                    if (value.I == 100)
                        return (value.L[0]).LI;
                }
            }
            catch (Exception e)
            {
                File.WriteAllText("Errors.txt",e.ToString());
            }
            
            return String.Empty;
        }

        public string GetGameId()
        {
            var ligaId = GetLigaId();
            if (ligaId == String.Empty)
                return ligaId;

            // Вынужденная мера. Буфер для Value
            JArray jArray = GetJsonFromLink("LiveFeed/Web_GetGames?lng=ru&champ=" + ligaId).Value;

            // Вынюхали пиздец
            if (jArray.Count == 0)
                return String.Empty;

            return ((dynamic)jArray[0]).GameId;
        }


        public string GetGameInfo(string gameID)
        {
            if (gameID == String.Empty)
                return string.Empty;

            return GetJsonFromLink("LiveFeed/GetGameZip?id=" + gameID).ToString();

        }


        public void Update()
        {
            var curStateString = GetGameInfo(GetGameId());

            if (curStateString == String.Empty)
            {
                TextUpdated("Пришло пустое состояние игры");
                return;
            }

            GameModel.Root gameState = GameModel.Root.FromJson(curStateString);

            if (gameState.Success == false)
            {
                TextUpdated("Сервер отказал в получении состояния игры (!)");
                return;
            }

            GameController.Update(gameState, this);
                    
        }


    }
}
