﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gandbolparser_sharp
{
    public class GameController
    {

        public List<Game> Games;

        public GameController()
        {
            Games = new List<Game>();
        }

        public void Update(GameModel.Root gameState, Parser parser)
        {
     

            // Init First game of the sequence
            if (Games.Count == 0)
            {
                Games.Add(new Game(gameState));
                parser.TextUpdated("Первая считанная игра\n\n");
            }

            // Switch to the next game 
            if (GetGameIndex(gameState) < Games.Last().GetLastGameIndex())
            {
                Games.Add(new Game(gameState));
                parser.TextUpdated("Переключение на следующую игру\n\n");
            }

            // Add state of the game 
            if (GetGameIndex(gameState) > Games.Last().GetLastGameIndex())
            {
                Games.Last().AddGameState(gameState, GetGameIndex(gameState));
                parser.TextUpdated(AppendGameInfoToString("Добавлено состояние игры", gameState));

                //if (GetGameIndex(gameState) == 1)
                //    parser.TelegramBot.SendMessage(AppendGameInfoToString("30 минута", gameState), 219370133);
            }

            UpdateStat(gameState, parser);    

        }



        private void UpdateStat(GameModel.Root gameState, Parser parserInstance)
        {
            parserInstance.UpdateStat("labelGameNameVal", Games.Last().Name);
            parserInstance.UpdateStat("labelGameScoreVal", Games.Last().GetScore());
            parserInstance.UpdateStat("labelGameTimeVal", (gameState.Value.SC.TS / 60).ToString());
        }

        private string AppendGameInfoToString(string text, GameModel.Root gameState )
        {
            return DateTime.Now.ToLongTimeString() + ",\n" + text + ",\nНомер игры: " + gameState.Value.I +  ",\nНазвание: " + Games.Last().Name + ",\nИгровое время: " + gameState.Value.SC.TS / 60 + ",\nСчет: " + Games.Last().GetScore()+"\n\n";
        }

        private int GetGameIndex(GameModel.Root gameState)
        {
            if (gameState.Value.SC.TS < 1800)
                return 0;

            if (gameState.Value.SC.TS >= 1800 && gameState.Value.SC.TS < 3600)
                return 1;

            return 2;

        }


    }
}
